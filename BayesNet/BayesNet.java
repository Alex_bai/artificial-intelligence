import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BayesNet {

	private List<Node> nodes;
	private Map<Node, String> vars;
	private double endPro;
	
	public BayesNet()
	{
		nodes = new ArrayList<Node>();
		vars = new HashMap<Node, String>();
	}
	
	public void readFile(String fileName)
	{		
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(new File(fileName)));
			String data = "";
			//get all the node names and values
			while(!(data=reader.readLine()).equals("# Parents"))
			{
				String temp[] = data.split(" ");
				String nodeName = temp[0].trim();
				setNodeValue(nodeName, temp);
			}			
			//get all the nodes and their parent node
			while(!(data=reader.readLine()).equals("# Tables"))
			{
				String temp[] = data.split(" ");
				String nodeName = temp[0].trim();
				setNodeParent(nodeName, temp);
			}
			//get all the node cpts
			while((data=reader.readLine())!=null)
			{
				Node node = findNodeByName(data.trim());
				int length = 1;
				for(int i=0;i<node.parents.size();i++)
				{
					Node parent = node.parents.get(i);
					length *= parent.values.length;
				}
				//initial node's CPT table
				String cpts[][] = new String[length][node.parents.size()+node.values.length];
				
				for(int row=0;row<cpts.length;row++)
				{
					data = reader.readLine();
					String temp[] = data.split(" ");
					double pro = 1;
					for(int col=0;col<temp.length;col++)
					{
						if(col>=node.parents.size())
						{
							pro -= Double.parseDouble(temp[col]);
						}						
						cpts[row][col] = temp[col];
					}
					cpts[row][temp.length] = String.valueOf(pro);
				}
				node.cpts = cpts;
			}						
		} catch (FileNotFoundException e) {			
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			try {
				reader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * create new node with node name and node values
	 * @param name
	 * @param temp
	 */
	public void setNodeValue(String name, String temp[])
	{
		String values[] = new String[temp.length-1];
		for(int i=0;i<values.length;i++)
		{
			values[i] = temp[i+1];
		}
		Node node = new Node(name,values);
		nodes.add(node);
	}
	
	/**
	 * set node parent with node name
	 * @param name
	 * @param temp
	 */
	public void setNodeParent(String name, String temp[])
	{
		Node node = findNodeByName(name);
		
		List<Node> parents = new ArrayList<Node>();
		for(int i=1;i<temp.length;i++)
		{
			Node parent = findNodeByName(temp[i]);
			parents.add(parent);
		}		
		node.parents = parents;
	}
	
	/**
	 * find node from nodes by node name
	 * @param name
	 * @return
	 */
	public Node findNodeByName(String name)
	{
		for(Node node : nodes)
		{
			if(node.name.equals(name))
			{
				return node;
			}
		}
		return null;
	}
	
	/**
	 * find index by value in the node
	 * @param node
	 * @param value
	 * @return
	 */
	public int findIndexByValue(Node node, String value)
	{
		int index = 0;
		for(int i=0;i<node.values.length;i++)
		{
			String temp = node.values[i];
			if(temp.equals(value))
			{
				index = i;
				break;
			}
		}
		return index;
	}
	
	/**
	 * generate the real value for each evidence node
	 * @param temp
	 */
	public void getEvidence(String temp)
	{
		String eviAndValue[] = temp.split("=");
		String evidence = eviAndValue[0].trim();
		String value = eviAndValue[1].trim();
		
		Node evidenceNode = findNodeByName(evidence);
		evidenceNode.realValueIndex = findIndexByValue(evidenceNode, value);
	}
	
	/**
	 * get all the probability
	 * @return
	 */
	public double getProbability()
	{
		double probability = 1;
		for(Node node : vars.keySet())
		{
			int index = findIndexByValue(node, vars.get(node));
			List<Node> parents = node.parents;
			
			if(parents == null)
			{
				probability = Double.parseDouble(node.cpts[0][parents.size()+index]);
			}else{
				for(int row=0;row<node.cpts.length;row++)
				{
					boolean flag = true;					
					for(int j=0;j<parents.size();j++)
					{
						Node parent = parents.get(j);
						if(!node.cpts[row][j].equals(vars.get(parent)))
						{
							flag = false;
							break;
						}
					}
					if(flag)
					{
						probability *= Double.parseDouble(node.cpts[row][parents.size()+index]);
						break;
					}
				}
			}
		}
		return probability;
	}
	
	
	/**
	 * enumerate all 
	 * @param index
	 */
	public void enumerateAll(int index)
	{
		if(index == nodes.size())
		{
			endPro += getProbability();			
			return;
		}
		Node current = nodes.get(index);
		//if value in evidence
		if(current.realValueIndex != -1)
		{
			vars.put(current, current.values[current.realValueIndex]);
			enumerateAll(index+1);
		}else{
			for(int i=0;i<current.values.length;i++)
			{
				vars.put(current, current.values[i]);
				enumerateAll(index+1);
			}			
		}
	}
	
	/**
	 * calculate the probability
	 * @param temp
	 */
	public void play(String temp)
	{
		DecimalFormat format = new DecimalFormat("0.000");		
		if(temp.contains("|"))
		{
			String data[] = temp.split("\\|");
			
			Node queryNode = findNodeByName(data[0].trim());
			
			String evis = data[1];
			
			String evi[] = evis.split(",");
			for(String e : evi)
			{
				getEvidence(e);
			}
			
			enumerateAll(0);
			double result = endPro;
			
			for(int i=0;i<queryNode.values.length;i++)
			{
				endPro = 0;
				queryNode.realValueIndex = i;
				enumerateAll(0);
				
				double value = endPro/result;				
				System.out.print("P("+queryNode.values[i]+") = "+format.format(value));
				if(i < queryNode.values.length-1)
				{
					System.out.print(", ");
				}
			}
		}else{
			Node queryNode = findNodeByName(temp);
			
			if(queryNode.parents == null)
			{
				for(int i=0;i<queryNode.values.length;i++)
				{
					double value = Double.parseDouble(queryNode.cpts[0][i]);
					System.out.print("P("+queryNode.values[i]+") = "+format.format(value));
					if(i < queryNode.values.length-1)
					{
						System.out.print(", ");
					}
				}
			}else{				
				for(int i=0;i<queryNode.values.length;i++)
				{
					endPro = 0;
					queryNode.realValueIndex = i;
					enumerateAll(0);
													
					System.out.print("P("+queryNode.values[i]+") = "+format.format(endPro));
					if(i < queryNode.values.length-1)
					{
						System.out.print(", ");
					}
				}
			}						
		}
	}
	
	/**
	 * clear the node and endPro
	 */
	public void clear()
	{
		for(Node node : nodes)
		{
			node.realValueIndex = -1;
		}
		endPro = 0;
	}	
}
