import java.util.ArrayList;
import java.util.List;

public class Node {

	public String name;
	public String values[];
	public int realValueIndex;	
	public List<Node> parents;
	public String cpts[][];	
	
	public Node(String name, String values[])
	{
		this.name = name;
		this.values = values;
		this.parents = new ArrayList<Node>();
		this.realValueIndex = -1;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String[] getValues() {
		return values;
	}

	public void setValues(String[] values) {
		this.values = values;
	}

	public List<Node> getParents() {
		return parents;
	}

	public void setParents(List<Node> parents) {
		this.parents = parents;
	}

	public String[][] getCpts() {
		return cpts;
	}

	public void setCpts(String[][] cpts) {
		this.cpts = cpts;
	}	
	
	public int getRealValueIndex() {
		return realValueIndex;
	}

	public void setRealValueIndex(int realValueIndex) {
		this.realValueIndex = realValueIndex;
	}
}
