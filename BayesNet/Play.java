import java.util.Scanner;

public class Play {

	public static void main(String[] args) {
 
		String fileName = args[0];
		
		BayesNet bn = new BayesNet();
		
		System.out.println("Loading file "+fileName+"\n");
		//read file 
		bn.readFile(fileName);
		
		Scanner scanner = new Scanner(System.in);		
		String temp = scanner.nextLine();
		
		while(!temp.equals("quit"))
		{
			bn.play(temp);
			bn.clear();
			System.out.println("\n");
			temp = scanner.nextLine();
		}		
	}
}
