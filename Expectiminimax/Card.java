
public class Card implements Comparable<Card>{
	public String rank;
	public String suit;
	public boolean isHigh;
	
	public Card(String rank, String suit)
	{
		this.rank = rank;
		this.suit = suit;
	}
	public String getRank() {
		return rank;
	}
	public int getIntRank()
	{
		int result = 0;
		if(this.rank.equals("J"))
		{
			result = 11;
		}else if(this.rank.equals("Q"))
		{
			result = 12;
		}else if(this.rank.equals("K"))
		{
			result = 13;
		}else if(this.rank.equals("A"))
		{
			if(isHigh)
			{
				result = 14;
			}else{
				result = 1;
			}			
		}else{
			result = Integer.parseInt(this.rank);
		}
		return result;
	}
	public void setHigh()
	{
		isHigh = true;
	}
	public void setLow(){
		isHigh = false;
	}
	public void setRank(String rank) {
		this.rank = rank;
	}
	public String getSuit() {
		return suit;
	}
	public void setSuit(String suit) {
		this.suit = suit;
	}		
	
	@Override
	public int compareTo(Card o) {
		// TODO Auto-generated method stub
		int data = this.getIntRank();
		int other = o.getIntRank();		
		return data-other;
	}	
	
	public String toString()
	{
		return rank+suit+" ";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (isHigh ? 1231 : 1237);
		result = prime * result + ((rank == null) ? 0 : rank.hashCode());
		result = prime * result + ((suit == null) ? 0 : suit.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Card other = (Card) obj;
		if (rank == null) {
			if (other.rank != null)
				return false;
		} else if (!rank.equals(other.rank))
			return false;
		if (suit == null) {
			if (other.suit != null)
				return false;
		} else if (!suit.equals(other.suit))
			return false;
		return true;
	}
	
}
