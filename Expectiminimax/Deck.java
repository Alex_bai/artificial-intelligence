import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Deck {

	public List<Card> cards;
	private Random random;
	
	public Deck()
	{
		random = new Random();
		createDeck();
	}
	
	private void createDeck()
	{
		cards = new ArrayList<Card>();
		String ranks[] = SuitAndRank.ranks;
		String suits[] = SuitAndRank.suits;
		for(String rank : ranks)
		{
			for(String suit : suits)
			{
				cards.add(new Card(rank, suit));
			}
		}
	}
	
	public Card pop()
	{
		return cards.remove(random.nextInt(cards.size()));
	}
}
