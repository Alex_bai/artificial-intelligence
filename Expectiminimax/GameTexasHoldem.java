import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class GameTexasHoldem {

	private Deck deck;
	private List<Card> tableCards;
	//the fifth card
	private Card realFifthCard;
	private Node parentNode;		
	 
	//all the leaf nodes in the global tree
	private List<Node> leaf;
	
	private int maxLevel;
	
	//store the leaf nodes on the first tree which means
	//they include all the all in nodes and call node that the 
	//rest money is 0
	private List<Node> noSecondTreeNodes;
	
	//store the parents of second chance first nodes.(the player still can
	//bet on the second chance node, so only these nodes have the second chance node trees)
	private List<Node> secondTreeParentNodes;	
	
	private int POT_MONEY;	
	private final int PLAYER_MONEY = 1500;
	private final int FIFTH_DENOMINATOR = 46;
	private final int OPPO_DENOMINATOR = 45*22;
	
	private final String CHECK = "check";
	private final String BET250 = "bet";
	private final String ALLIN = "allIn";
	private final String CALL = "call";
	private final String RAISEDOUBLE = "raiseDouble";	
	private final String FOLD = "fold";
	private final String CHANCE_NODE = "chanceNode";
		
	private String rationalFlag;
	private String verboseFlag;
	private Player firstPlayer;
	private Player secondPlayer;
	//the rest money of player1
	private int firstRest;
	//the rest money of player2
	private int secondRest;
	//the number hands of player1 win
	private int firstWinnedNum;
	//the number hands of player1 lose
	private int firstLostNum;
	//the number hands of player2 win
	private int secondWinnedNum;
	//the number hands of player2 lose
	private int secondLostNum;
	//the number hands of player draw
	private int drawNum;
	//the total money that player1 win
	private int firstTotalWinned;
	//the total money that player1 win
	private int secondTotalWinned;
	//the fold node
	private Node doFold;
	//hands number
	private int hands;
	
	public GameTexasHoldem(String rationalFlag, String verboseFlag)
	{		
		this.rationalFlag = rationalFlag;
		this.verboseFlag = verboseFlag;
		this.hands = 0;
	}
	
	/**
	 * new the game
	 * @param deck
	 * @param firstPlayer
	 * @param secondPlayer
	 */
	public void newGame(Deck deck, Player firstPlayer, Player secondPlayer )
	{		
		this.POT_MONEY = 500;
		this.deck = deck;
		this.tableCards = new ArrayList<Card>();
		this.secondTreeParentNodes = new ArrayList<Node>();
		this.noSecondTreeNodes = new ArrayList<Node>();			
		this.leaf = new ArrayList<Node>();
		this.firstPlayer = firstPlayer;
		this.secondPlayer = secondPlayer;
		this.doFold = null;
		this.realFifthCard = null;
	}
	/**
	 * start the game
	 */
	public void startGame()
	{	
		if(verboseFlag.equals(""))
		{
			//pop 4 cards on the table
			for(int i=0;i<4;i++)
			{
				Card card = deck.pop();				
				tableCards.add(card);			
			}			
		}else{
			System.out.print("Table: ");		
			//pop 4 cards on the table
			for(int i=0;i<4;i++)
			{
				Card card = deck.pop();
				System.out.print(card.toString());
				tableCards.add(card);			
			}
			System.out.println();
			System.out.println("Pot: $"+POT_MONEY);
			System.out.println();
			System.out.println("Player"+firstPlayer.number+": Acts first\n");						
		}	
		//generate the tree
		generateTree();
		
		if(rationalFlag.equals("r"))
		{
			doRationalAction();
		}else{
			doNonRationalAction();
		}
	}
	
	/**
	 * do rational action
	 */
	public void doRationalAction()
	{
		if(verboseFlag.equals("v"))
		{
			doVerboseAction(parentNode, firstPlayer.number);
			System.out.println();				
			//print two players' result and check ranking
			printAndCheck(firstPlayer, secondPlayer);													
		}else{
			doNonVerboseAction(parentNode, firstPlayer.number);
			//print two players' result and check ranking
			printAndCheckNonVerbose(firstPlayer, secondPlayer);
		}
	}
	
	/**
	 * do rational and verbose action
	 * @param node parent node
	 * @param flag firstPlayer's number
	 */
	public void doVerboseAction(Node node, int flag)
	{		
		List<Node> children = node.children;
		//if children is empty then jump the recursive
		if(children.isEmpty() || children == null)
		{		
			//if the end node is belong to player1
			if(node.belongs == 1)
			{
				firstRest = node.restMoney;
				secondRest = node.parent.restMoney;
			}else{
				firstRest = node.parent.restMoney;
				secondRest = node.restMoney;
			}
						
		}else if(children.get(0).name.equals(CHANCE_NODE))		//if the children is the fifth card node
		{
			System.out.println();
			//first remove the second player's cards from deck
			deck.cards.removeAll(secondPlayer.cards);
			//get the fifth card
			realFifthCard = deck.pop();		
			//get current node's rest money and opponent node's rest money
			int curr_restMoney = node.restMoney;
			int oppo_restMoney = node.parent.restMoney;
			
			System.out.print("Player"+firstPlayer.number+": ");
			for(Card card : firstPlayer.cards)
			{
				System.out.print(card.toString());
			}			
			System.out.println("$"+curr_restMoney);
			
			System.out.print("Player"+secondPlayer.number+": ");
			for(Card card : secondPlayer.cards)
			{
				System.out.print(card.toString());
			}
			System.out.println("$"+oppo_restMoney);
			
			//print table cards
			System.out.print("Table: ");
			for(Card card : tableCards)
			{
				System.out.print(card.toString());
			}
			System.out.println(realFifthCard.toString());
			
			POT_MONEY += (PLAYER_MONEY-curr_restMoney)+(PLAYER_MONEY-oppo_restMoney);
			
			System.out.println("Pot: $"+POT_MONEY+"\n");
			
			//find the fifth card in the card nodes
			for(Node child : children)
			{
				if(child.fifthCard.equals(realFifthCard))
				{
					doVerboseAction(child, firstPlayer.number);
					break;
				}
			}
		}else{
			Node temp = null;
			if(flag == firstPlayer.number)
			{
				temp = getMaxMoneyNode(node.children);
			}else{
				temp = getMinMoneyNode(node.children);
			}
			System.out.print("Player"+flag+":  "+temp.name);
			
			if(temp.name.equals(FOLD))
			{			
				doFold = temp;
				System.out.println();
				return;			
			}else if(temp.name.equals(CHECK))
			{
				System.out.println();
				doVerboseAction(temp, 3-flag);
			}else if(temp.name.equals(ALLIN))
			{
				int betMoney = (PLAYER_MONEY*2+500-POT_MONEY)/2;
				temp.betMoney = betMoney;
				System.out.println(" $"+betMoney);
				doVerboseAction(temp, 3-flag);
			}else if(temp.name.equals(BET250))
			{
				temp.betMoney = 250;
				System.out.println(" $250");
				doVerboseAction(temp, 3-flag);
			}else if(temp.name.equals(CALL))
			{
				int betMoney = node.betMoney;
				temp.betMoney = betMoney;
				System.out.println(" $"+betMoney);
				doVerboseAction(temp, 3-flag);
			}else{
				int betMoney = PLAYER_MONEY-temp.restMoney;
				temp.betMoney = betMoney;
				System.out.println(" $"+betMoney);
				doVerboseAction(temp, 3-flag);
			}											
		}
	}
	
	/**
	 * do rational non-verbose action
	 * @param node the parent node
	 * @param flag the first player's number
	 */
	public void doNonVerboseAction(Node node, int flag)
	{		
		List<Node> children = node.children;
		//if children is empty then jump the recursive
		if(children.isEmpty() || children == null)
		{		
			//if the node is belongs to player1
			if(node.belongs == 1)
			{
				firstRest = node.restMoney;
				secondRest = node.parent.restMoney;
			}else{
				firstRest = node.parent.restMoney;
				secondRest = node.restMoney;
			}
						
		}else if(children.get(0).name.equals(CHANCE_NODE))		//if the node is card node
		{
			deck.cards.removeAll(secondPlayer.cards);
			//get the fifth card
			realFifthCard = deck.pop();			
			int curr_restMoney = node.restMoney;
			int oppo_restMoney = node.parent.restMoney;
			
			POT_MONEY += (PLAYER_MONEY-curr_restMoney)+(PLAYER_MONEY-oppo_restMoney);
			//find the fifth card in the card nodes
			for(Node child : children)
			{
				if(child.fifthCard.equals(realFifthCard))
				{
					doNonVerboseAction(child, firstPlayer.number);
					break;
				}
			}
		}else{
			Node temp = null;
			if(flag == firstPlayer.number)
			{
				//get max money
				temp = getMaxMoneyNode(node.children);
			}else{
				//get min money
				temp = getMinMoneyNode(node.children);
			}
			
			if(temp.name.equals(FOLD))
			{			
				doFold = temp;
				return;			
			}else{
				doNonVerboseAction(temp, 3-flag);
			}			
		}
	}
	
	/**
	 * print and check non-verbose
	 * @param curr first player
	 * @param oppo second player
	 */
	public void printAndCheckNonVerbose(Player curr, Player oppo)
	{
		int winned = 0;
		int lost = 0;
		//if the player fold the game
		if(doFold != null)
		{
			winned = PLAYER_MONEY-doFold.restMoney+500;
			lost = winned-500;
			
			if(curr.number == 1)
			{
				if(doFold.belongs == curr.number)
				{
					System.out.printf("%10d%20d%20d\n", (++hands), -lost, winned);	
					
					firstLostNum++;
					secondWinnedNum++;
					firstTotalWinned += -lost;
					secondTotalWinned += winned;
				}else{
					System.out.printf("%10d%20d%20d\n", (++hands), winned, -lost);
					
					firstWinnedNum++;
					secondLostNum++;
					firstTotalWinned += winned;
					secondTotalWinned += -lost;
				}	
			}else{
				if(doFold.belongs == curr.number)
				{
					System.out.printf("%10d%20d%20d\n", (++hands), -lost, winned);	
					
					firstWinnedNum++;
					secondLostNum++;
					firstTotalWinned += winned;
					secondTotalWinned += -lost;
				}else{
					System.out.printf("%10d%20d%20d\n", (++hands), winned, -lost);
					
					firstLostNum++;
					secondWinnedNum++;
					firstTotalWinned += -lost;
					secondTotalWinned += winned;
				}								
			}		
		}else{
			List<Card> curr_cards = new ArrayList<Card>();
			curr_cards.addAll(tableCards);
			curr_cards.add(realFifthCard);
			curr_cards.addAll(curr.cards);
			
			RankingUtil.checkRanking(curr, curr_cards);			
			RankingEnum rankingEnum = curr.rankingEnum;
			List<Card> rankingList = curr.rankingList;
			
			List<Card> oppo_cards = new ArrayList<Card>();
			oppo_cards.addAll(tableCards);
			oppo_cards.add(realFifthCard);
			oppo_cards.addAll(oppo.cards);
			
			RankingUtil.checkRanking(oppo, oppo_cards);			
			RankingEnum oppo_enum = oppo.rankingEnum;
			List<Card> oppo_ranking = oppo.rankingList;
			
			//check the card
			double coefficient = checkRanking(rankingList, rankingEnum, oppo_ranking, oppo_enum, 
					curr_cards, oppo_cards);
					
			if(coefficient == 1)
			{		
				if(curr.number == 1)
				{
					winned = PLAYER_MONEY-secondRest+500;
					lost = winned-500;
					
					firstWinnedNum++;
					secondLostNum++;
					firstTotalWinned += winned;
					secondTotalWinned += -lost;
					
					System.out.printf("%10d%20d%20d\n", (++hands), winned, -lost);
				}else{
					winned = PLAYER_MONEY-firstRest+500;
					lost = winned-500;
					
					firstLostNum++;
					secondWinnedNum++;
					firstTotalWinned += -lost;
					secondTotalWinned += winned;
					
					System.out.printf("%10d%20d%20d\n", (++hands), -lost, winned);
				}			
				
			}else if(coefficient == -1)
			{
				if(curr.number == 1)
				{
					winned = PLAYER_MONEY-firstRest+500;
					lost = winned-500;
					
					firstLostNum++;
					secondWinnedNum++;
					firstTotalWinned += -lost;
					secondTotalWinned += winned;
					
					System.out.printf("%10d%20d%20d\n", (++hands), -lost, winned);
				}else{
					winned = PLAYER_MONEY-secondRest+500;
					lost = winned-500;
					
					firstWinnedNum++;
					secondLostNum++;
					firstTotalWinned += winned;
					secondTotalWinned += -lost;
					
					System.out.printf("%10d%20d%20d\n", (++hands), winned, -lost);					
				}			
			}else{
				winned = POT_MONEY/2;		
				System.out.printf("%10d%20d%20d\n", (++hands), winned, winned);
				drawNum++;
				firstTotalWinned += winned;
				secondTotalWinned += winned;
			}		
		}		
	}
	
	/**
	 * print and check
	 * @param curr first player
	 * @param oppo second player
	 */
	public void printAndCheck(Player curr, Player oppo)
	{
		int winned = 0;
		int lost = 0;
		//if the player has fold the game
		if(doFold != null)
		{
			winned = PLAYER_MONEY-doFold.restMoney+500;
			lost = winned-500;
			
			if(curr.number == 1)
			{
				if(doFold.belongs == curr.number)
				{
					System.out.println("Player1: Lose   $"+lost);
					System.out.println("Player2: Win    $"+winned);
					
					firstLostNum++;
					secondWinnedNum++;
					firstTotalWinned += -lost;
					secondTotalWinned += winned;
				}else{
					System.out.println("Player1: Win    $"+winned);
					System.out.println("Player2: Lose   $"+lost);
					
					firstWinnedNum++;
					secondLostNum++;
					firstTotalWinned += winned;
					secondTotalWinned += -lost;
				}								
			}else{
				if(doFold.belongs == curr.number)
				{					
					System.out.println("Player1: Lose   $"+lost);
					System.out.println("Player2: Win    $"+winned);
					
					firstLostNum++;
					secondWinnedNum++;
					firstTotalWinned += -lost;
					secondTotalWinned += winned;
				}else{
					System.out.println("Player1: Win    $"+winned);
					System.out.println("Player2: Lose   $"+lost);
					
					firstWinnedNum++;
					secondLostNum++;
					firstTotalWinned += winned;
					secondTotalWinned += -lost;
				}				
			}		
		}else{
			List<Card> curr_cards = new ArrayList<Card>();
			curr_cards.addAll(tableCards);
			curr_cards.add(realFifthCard);
			curr_cards.addAll(curr.cards);
			
			RankingUtil.checkRanking(curr, curr_cards);			
			RankingEnum rankingEnum = curr.rankingEnum;
			List<Card> rankingList = curr.rankingList;
			System.out.println("Player"+curr.number+": "+rankingEnum+" "+rankingList.toString());	
			
			List<Card> oppo_cards = new ArrayList<Card>();
			oppo_cards.addAll(tableCards);
			oppo_cards.add(realFifthCard);
			oppo_cards.addAll(oppo.cards);
			
			RankingUtil.checkRanking(oppo, oppo_cards);			
			RankingEnum oppo_enum = oppo.rankingEnum;
			List<Card> oppo_ranking = oppo.rankingList;
			System.out.println("Player"+oppo.number+": "+oppo_enum+" "+oppo_ranking.toString());	
			
			//check the cards
			double coefficient = checkRanking(rankingList, rankingEnum, oppo_ranking, oppo_enum, 
					curr_cards, oppo_cards);
					
			if(coefficient == 1)
			{		
				if(curr.number == 1)
				{
					winned = PLAYER_MONEY-secondRest+500;
					lost = winned-500;
					
					firstWinnedNum++;
					secondLostNum++;
					firstTotalWinned += winned;
					secondTotalWinned += -lost;
					
					System.out.println("Player1: Win    $"+winned);
					System.out.println("Player2: Lose   $"+lost);
				}else{
					winned = PLAYER_MONEY-firstRest+500;
					lost = winned-500;
					
					firstLostNum++;
					secondWinnedNum++;
					firstTotalWinned += -lost;
					secondTotalWinned += winned;
					
					System.out.println("Player1: Lose   $"+lost);
					System.out.println("Player2: Win    $"+winned);
				}												
			}else if(coefficient == -1)
			{
				if(curr.number == 1)
				{
					winned = PLAYER_MONEY-firstRest+500;
					lost = winned-500;
					
					firstLostNum++;
					secondWinnedNum++;
					firstTotalWinned += -lost;
					secondTotalWinned += winned;
					
					System.out.println("Player1: Lose   $"+lost);
					System.out.println("Player2: Win    $"+winned);
				}else{
					winned = PLAYER_MONEY-secondRest+500;
					lost = winned-500;
					
					firstWinnedNum++;
					secondLostNum++;
					firstTotalWinned += winned;
					secondTotalWinned += -lost;
					
					System.out.println("Player1: Win    $"+winned);
					System.out.println("Player2: Lose   $"+lost);
				}			
			}else{
				winned = POT_MONEY/2;			
				System.out.println("Player"+curr.number+": draw   $"+winned);
				System.out.println("Player"+oppo.number+": draw   $"+winned);
				drawNum++;
				firstTotalWinned += winned;
				secondTotalWinned += winned;
			}		
		}		
	}
	
	/**
	 * print all the hands' result
	 */
	public void printResult()
	{
		System.out.println("----------------");
		System.out.println(" Result ");
		System.out.println("----------------");
		
		System.out.print("Player1: "+firstWinnedNum+" win, "+firstLostNum+" lose, "+drawNum+" draw:");
		if(firstTotalWinned > 0)
		{
			System.out.println("+"+firstTotalWinned);
		}else{
			System.out.println(firstTotalWinned);
		}
		
		System.out.print("Player2: "+secondWinnedNum+" win, "+secondLostNum+" lose: "+drawNum+" draw:");
		if(secondTotalWinned > 0)
		{
			System.out.println("+"+secondTotalWinned);
		}else{
			System.out.println(secondTotalWinned);
		}
	}
	
	/**
	 * do non-rational action
	 */
	public void doNonRationalAction()
	{
		if(verboseFlag.equals("v"))
		{
			doNonRationalVerbose(parentNode, firstPlayer.number, true);
			System.out.println();				
			//print two players' result and check ranking
			printAndCheck(firstPlayer, secondPlayer);													
		}else{
			doNonRationalNonVerbose(parentNode, firstPlayer.number, true);
			//print two players' result and check ranking
			printAndCheckNonVerbose(firstPlayer, secondPlayer);
		}
	}
	
	/**
	 * do non-rational and non-verbose action
	 * @param node parent node
	 * @param flag 
	 * @param isFirst first play
	 */
	public void doNonRationalNonVerbose(Node node, int flag, boolean isFirst)
	{
		List<Node> children = node.children;
		//judge whether the node is leaf node
		if(children.isEmpty() || children == null)
		{		
			if(node.belongs == 1)
			{
				firstRest = node.restMoney;
				secondRest = node.parent.restMoney;
			}else{
				firstRest = node.parent.restMoney;
				secondRest = node.restMoney;
			}
						
		}else if(children.get(0).name.equals(CHANCE_NODE))	//if the children are card nodes
		{
			deck.cards.removeAll(secondPlayer.cards);
			//get the fifth card
			realFifthCard = deck.pop();			
			int curr_restMoney = node.restMoney;
			int oppo_restMoney = node.parent.restMoney;
			
			POT_MONEY += (PLAYER_MONEY-curr_restMoney)+(PLAYER_MONEY-oppo_restMoney);
			
			//find the fifth card in the card nodes
			for(Node child : children)
			{
				if(child.fifthCard.equals(realFifthCard))
				{
					doNonRationalNonVerbose(child, firstPlayer.number, true);
					break;
				}
			}
		}else{
			Node temp = null;			
			if(firstPlayer.isRational)
			{				
				if(flag == firstPlayer.number)
				{
					temp = getMaxMoneyNode(node.children);
				}else{
					//get all the cards on the table and on hand, total is 6 cards
					List<Card> cards = new ArrayList<Card>();
					cards.addAll(tableCards);
					cards.addAll(secondPlayer.cards);
					if(realFifthCard != null)
					{
						cards.add(realFifthCard);
					}
					//get the node based on non-rational rule
					temp = getNonRaitonalMoneyNode(secondPlayer,node, cards, false);
				}	 				
			}else{
				if(flag == firstPlayer.number)
				{
					//get all the cards on the table and on hand, total is 6 cards
					List<Card> cards = new ArrayList<Card>();
					cards.addAll(tableCards);
					cards.addAll(firstPlayer.cards);
					if(realFifthCard != null)
					{
						cards.add(realFifthCard);
					}
					//get the node based on non-rational rule
					temp = getNonRaitonalMoneyNode(firstPlayer,node, cards, isFirst);					
				}else{
					temp = getMinMoneyNode(node.children);
				}					
			}			
			if(temp.name.equals(FOLD))
			{			
				doFold = temp;
				return;			
			}else{
				doNonRationalNonVerbose(temp, 3-flag, false);
			}	
		}
	}
	
	/**
	 * do non-rational and verbose action
	 * @param node
	 * @param flag
	 * @param isFirst
	 */
	public void doNonRationalVerbose(Node node, int flag, boolean isFirst)
	{
		List<Node> children = node.children;
		//judge whether the node is leaf node
		if(children.isEmpty() || children == null)
		{		
			if(node.belongs == 1)
			{
				firstRest = node.restMoney;
				secondRest = node.parent.restMoney;
			}else{
				firstRest = node.parent.restMoney;
				secondRest = node.restMoney;
			}
						
		}else if(children.get(0).name.equals(CHANCE_NODE))		//if the children are card nodes
		{
			System.out.println();			
			deck.cards.removeAll(secondPlayer.cards);
			//get the fifth card
			realFifthCard = deck.pop();			
			int curr_restMoney = node.restMoney;
			int oppo_restMoney = node.parent.restMoney;
			
			System.out.print("Player"+firstPlayer.number+": ");
			for(Card card : firstPlayer.cards)
			{
				System.out.print(card.toString());
			}			
			System.out.println("$"+curr_restMoney);
			
			System.out.print("Player"+secondPlayer.number+": ");
			for(Card card : secondPlayer.cards)
			{
				System.out.print(card.toString());
			}
			System.out.println("$"+oppo_restMoney);
			
			//print table cards
			System.out.print("Table: ");
			for(Card card : tableCards)
			{
				System.out.print(card.toString());
			}
			System.out.println(realFifthCard.toString());
			
			POT_MONEY += (PLAYER_MONEY-curr_restMoney)+(PLAYER_MONEY-oppo_restMoney);
			
			System.out.println("Pot: $"+POT_MONEY+"\n");
			
			for(Node child : children)
			{
				if(child.fifthCard.equals(realFifthCard))
				{
					doNonRationalVerbose(child, firstPlayer.number, true);
					break;
				}
			}
		}else{
			Node temp = null;
			if(firstPlayer.isRational)
			{				
				if(flag == firstPlayer.number)
				{
					temp = getMaxMoneyNode(node.children);
				}else{
					//get all the cards on the table and on hand, total is 6 cards
					List<Card> cards = new ArrayList<Card>();
					cards.addAll(tableCards);
					cards.addAll(secondPlayer.cards);
					if(realFifthCard != null)
					{
						cards.add(realFifthCard);
					}
					temp = getNonRaitonalMoneyNode(secondPlayer,node, cards, false);
				}
	 				
			}else{
				if(flag == firstPlayer.number)
				{
					//get all the cards on the table and on hand, total is 6 cards
					List<Card> cards = new ArrayList<Card>();
					cards.addAll(tableCards);
					cards.addAll(firstPlayer.cards);
					if(realFifthCard != null)
					{
						cards.add(realFifthCard);
					}
					temp = getNonRaitonalMoneyNode(firstPlayer,node, cards, isFirst);					
				}else{
					temp = getMinMoneyNode(node.children);
				}					
			}
			System.out.print("Player"+flag+":  "+temp.name);
			
			if(temp.name.equals(FOLD))
			{			
				doFold = temp;
				System.out.println();
				return;			
			}else if(temp.name.equals(CHECK))
			{
				System.out.println();
				doNonRationalVerbose(temp, 3-flag, false);
			}else if(temp.name.equals(ALLIN))
			{
				int betMoney = (PLAYER_MONEY*2+500-POT_MONEY)/2;
				temp.betMoney = betMoney;
				System.out.println(" $"+betMoney);
				doNonRationalVerbose(temp, 3-flag, false);
			}else if(temp.name.equals(BET250))
			{
				temp.betMoney = 250;
				System.out.println(" $250");
				doNonRationalVerbose(temp, 3-flag, false);
			}else if(temp.name.equals(CALL))
			{
				int betMoney = node.betMoney;
				temp.betMoney = betMoney;
				System.out.println(" $"+betMoney);
				doNonRationalVerbose(temp, 3-flag, false);
			}else{
				int betMoney = PLAYER_MONEY-temp.restMoney;
				temp.betMoney = betMoney;
				System.out.println(" $"+betMoney);
				doNonRationalVerbose(temp, 3-flag, false);
			}	
		}
	}
	
	/**
	 * the non rational player is the second to play 
	 * @param children
	 * @param cards
	 */
	public Node getNonRaitonalMoneyNode(Player player, Node node, List<Card> cards, boolean isFirstPlayer)
	{
		List<Node> children = node.children;
		//check if the ranking list of the cards
		RankingUtil.checkRanking(player, cards);
		RankingEnum rankingEnum = player.rankingEnum;
		//if the player has a hand that is worse than two pair
		if(rankingEnum.ordinal()<RankingEnum.TWO_PAIR.ordinal())
		{
			//if they are the first to bet, they will always check
			if(isFirstPlayer)
			{
				for(Node temp : children)
				{
					if(temp.name.equals(CHECK))
					{
						return temp;
					}
				}
			}else{
				//if player1 has already checked, they will always check
				if(node.name.equals(CHECK))
				{
					for(Node temp : children)
					{
						if(temp.name.equals(CHECK))
						{
							return temp;
						}
					}
				}else if(node.name.equals(BET250)){	//if player1 bets, they will always call with any single pair
					if(rankingEnum.ordinal()==RankingEnum.ONE_PAIR.ordinal())
					{
						for(Node temp : children)
						{
							if(temp.name.equals(CALL))
							{
								return temp;
							}
						}
					}else{
						for(Node temp : children)
						{
							if(temp.name.equals(FOLD))
							{
								return temp;
							}
						}
					}
				}else if(node.name.equals(ALLIN))
				{
					if(rankingEnum.ordinal()==RankingEnum.ONE_PAIR.ordinal())
					{
						for(Node temp : children)
						{
							if(temp.name.equals(ALLIN))
							{
								return temp;
							}
						}
					}else{
						for(Node temp : children)
						{
							if(temp.name.equals(FOLD))
							{
								return temp;
							}
						}
					}
				}
			}
		}else if(rankingEnum.ordinal() >= RankingEnum.TWO_PAIR.ordinal()){
			if(isFirstPlayer)
			{
				for(Node temp : children)
				{
					if(temp.name.equals(BET250))
					{
						return temp;
					}
				}
			}else{
				if(node.name.equals(CHECK))
				{
					for(Node temp : children)
					{
						if(temp.name.equals(BET250))
						{
							return temp;
						}
					}
				}else{
					//if player1 bets or raises, player2 will always call
					Node res = null;
					for(Node temp : children)
					{
						if(temp.name.equals(CALL))
						{
							res = temp;
							break;
						}
					}
					if(res != null)
					{
						return res;
					}else{
						for(Node temp : children)
						{
							if(temp.name.equals(ALLIN))
							{
								return temp;
							}
						}
					}
				}
			}
		}
		return null;
	}	
	
	/**
	 * generate the tree
	 */
	public void generateTree()
	{
		parentNode = new Node("parent",null,PLAYER_MONEY, 0, 0, 0);		
		
		//get first tree
		generateTreeNodes(PLAYER_MONEY, parentNode, 1, 1, null);		
		
		//add chance node to leaf node in the first tree
		generateFifthCard(firstPlayer, secondPlayer);		
		
		//get second tree
		getSecondTreeNodes(1);			
		
		//get all the leaf nodes in the second tree
		for(Node node : secondTreeParentNodes)
		{
			List<Node> children = node.children;
			for(Node child : children)
			{
				getLeafNodes(child);
			}				
		}
		//get all the leaf nodes in the global tree
		for(Node node : noSecondTreeNodes)
		{
			List<Node> children = node.children;
			leaf.addAll(children);
		}

		//add player2's cards to deck, so the number of card is 46
		deck.cards.addAll(secondPlayer.cards);
		
		//generate the wins or lose money for the leaf nodes in the second tree 
		for(int i=0;i<leaf.size();i++)
		{
			Node node = leaf.get(i);
			Card fifthCard = node.fifthCard;
			//delete the card from deck
			deck.cards.remove(fifthCard);
		
			generateWinsMoney(firstPlayer, secondPlayer, node, fifthCard);
			
			//add the card into deck
			deck.cards.add(fifthCard);
		}				
		//get maximum level in the global tree
		getMaxLevel(leaf);
		
		//calculate maximum value and minimum value for each node
		calculateMaxAndMin();
	}
	
	/**
	 * calculate maximum value and minimum value for each node
	 */
	public void calculateMaxAndMin()
	{
		//if the level is 10, then the end level is 9, so we can first find leve 8,
		//and find all the nodes' children
		for(int i=maxLevel-1;i>=0;i--)
		{
			//get all the nodes in this level
			List<Node> nodes = new ArrayList<Node>();
			getNodesBasedOnLevel(nodes, parentNode, i);
			
			for(Node node : nodes)
			{				
				List<Node> children = node.children;
				//judge whether the node is leaf node
				if(children==null || children.isEmpty())
				{
					if(node.name.equals(FOLD))
					{
						node.minMoney = node.restMoney;
						node.maxMoney = node.restMoney;
					}else{
						node.minMoney = node.winnedMoney;
						node.maxMoney = node.winnedMoney;
					}					
				}else{
					//judge whether the children nodes are card nodes
					if(children.get(0).name.equals(CHANCE_NODE))
					{
						for(Node child : children)
						{
							if(child.children == null || child.children.isEmpty())
							{
								node.minMoney += child.winnedMoney/FIFTH_DENOMINATOR;
								node.maxMoney += child.winnedMoney/FIFTH_DENOMINATOR;							
							}else{								
								node.minMoney += child.minMoney/FIFTH_DENOMINATOR;
								node.maxMoney += child.maxMoney/FIFTH_DENOMINATOR;
							}						
						}
					}else{
						node.minMoney = getMinMoneyNode(children).maxMoney;
						node.maxMoney = getMaxMoneyNode(children).minMoney;
					}					
				}
			}								
		}
	}
	
	/**
	 * get minimum node from children nodes 
	 * @param nodes
	 * @return
	 */
	public Node getMinMoneyNode(List<Node> nodes)
	{	
		Node minNode = nodes.get(0);		
		
		for(Node temp : nodes)
		{
			if(minNode.maxMoney > temp.maxMoney)
			{
				minNode = temp;
			}
		}
		return minNode;
	}
	
	/**
	 * get maximum node from children nodes
	 * @param nodes
	 * @return
	 */
	public Node getMaxMoneyNode(List<Node> nodes)
	{		
		Node maxNode = nodes.get(0);		
		
		for(Node temp : nodes)
		{
			if(maxNode.minMoney < temp.minMoney)
			{
				maxNode = temp;
			}
		}
		return maxNode;
	}
	
	/**
	 * get all the node in the level
	 */
	public List<Node> getNodesBasedOnLevel(List<Node> nodes, Node node, int level)
	{		
		if(node != null)
		{
			if(node.level == level)
			{
				nodes.add(node);
			}
			
			List<Node> children = node.children;
							
			if(children==null || children.isEmpty())
			{
				return null;
			}else{				
				for(Node temp : children)
				{
					getNodesBasedOnLevel(nodes, temp, level);
				}
			}
		}	
		
		return nodes;
	}
	
	/**
	 * get maximum level in the global tree
	 * @param node
	 */
	public void getMaxLevel(List<Node> nodes)
	{
		if(nodes == null || nodes.isEmpty())
		{
			return;
		}else{
			for(Node temp : nodes)
			{				
				if(temp.level > maxLevel)
				{
					maxLevel = temp.level;
				}
			}
		}		
	}
	
	/** deck has some problems
	 * generate the wins money for each node
	 * @param current
	 * @param opponent
	 * @param node
	 */
	public void generateWinsMoney(Player current, Player opponent, Node node, Card fifthCard)	
	{	
		//get total money in the pot				
		int loseMoney =  PLAYER_MONEY-node.restMoney;
		int winsMoney = (PLAYER_MONEY-node.parent.restMoney) + POT_MONEY;
		int drawMoney = ((loseMoney+winsMoney)/2+node.restMoney) - PLAYER_MONEY;
		
		double coefficient = 1;		
		double totalMoney = 0;
		
		List<Card> onHand = new ArrayList<Card>();
		onHand.addAll(tableCards);		//4 cards
		onHand.add(fifthCard);			//1 card
		onHand.addAll(current.cards);	//2 cards
		
		//get ranking list and rankingEnum of player1
		RankingUtil.checkRanking(current, onHand);
		List<Card> curr_ranking = current.rankingList;
		RankingEnum curr_rankingEnum = current.rankingEnum;			
		
		//get ranking list and rankingEnum of player2
		List<Card> oppoHand = new ArrayList<Card>();
		for(int i=0;i<deck.cards.size()-1;i++)
		{
			Card first = deck.cards.get(i);
			for(int j=i+1;j<deck.cards.size();j++)
			{
				Card second = deck.cards.get(j);
				
				//7 cards in this list
				oppoHand.addAll(tableCards);		//4 cards		
				oppoHand.add(fifthCard);			//1 card
				oppoHand.add(first);				//1 card
				oppoHand.add(second);			//1 card
						
				RankingUtil.checkRanking(opponent, oppoHand);
				List<Card> oppo_ranking = opponent.rankingList;
				RankingEnum oppo_rankingEnum = opponent.rankingEnum;

				//check the rank of the cards
				coefficient = checkRanking(curr_ranking, curr_rankingEnum, oppo_ranking, 
						oppo_rankingEnum, onHand, oppoHand);
				
				oppoHand.clear();
				if(coefficient == -1)
				{
					totalMoney += (coefficient*loseMoney)/OPPO_DENOMINATOR;
				}else if(coefficient == 1){
					totalMoney += (coefficient*winsMoney)/OPPO_DENOMINATOR;
				}else{
					totalMoney += (coefficient*drawMoney)/OPPO_DENOMINATOR;
				}				
			}
		}	
		node.setWinnedMoney(totalMoney);
	}
	
	/**
	 * check ranking list
	 * @param curr_ranking
	 * @param curr_rankingEnum
	 * @param oppo_ranking
	 * @param oppo_rankingEnum
	 * @param curr_cards
	 * @param oppo_cards
	 * @return
	 */
	public double checkRanking(List<Card> curr_ranking, RankingEnum curr_rankingEnum, 
			List<Card> oppo_ranking, RankingEnum oppo_rankingEnum,
			List<Card> curr_cards, List<Card> oppo_cards)
	{
		double coefficient = 0;			
		
		if(curr_rankingEnum.ordinal() > oppo_rankingEnum.ordinal())
		{
			coefficient = 1;
		}else if(curr_rankingEnum.ordinal() < oppo_rankingEnum.ordinal())
		{
			//set minus money to node
			coefficient = -1;
		}else{
			int n = checkHighSequence(curr_ranking, oppo_ranking);
			if(n > 0)
			{
				coefficient = 1;
			}else if(n < 0)
			{
				coefficient = -1;
			}else{
				//if the ranking is the same, and sequence is the same, 
				//then get the highest card in the ranking list
				
				Collections.sort(curr_ranking);
				Collections.sort(oppo_ranking);
				
				Card curr_card = curr_ranking.get(curr_ranking.size()-1);
				Card oppo_card = oppo_ranking.get(oppo_ranking.size()-1);
				
				int m = checkHighCard(curr_card, oppo_card);
				
				if(m > 0)
				{
					coefficient = 1;
				}else if(m < 0)
				{
					coefficient = -1;
				}else{
					//the example is player1: A A A K 2
					//player2 : A A A 10 J
					//the high card is the same A
					int result = compareRestCards(curr_cards, curr_ranking, oppo_cards, oppo_ranking);
					if(result > 0)
					{
						coefficient = 1;
					}else if(result < 0)
					{
						coefficient = -1;
					}else{
						coefficient = 0.5;
					}							
				}						
			}	
		}
		return coefficient;
	}
	
	/**
	 * compare the rest cards
	 * @param onHand
	 * @param curr_ranking
	 * @param oppoHand
	 * @param oppo_ranking
	 * @return
	 */
	public int compareRestCards(List<Card> onHand, List<Card> curr_ranking, List<Card> oppoHand, List<Card> oppo_ranking)
	{
		List<Card> curr = new ArrayList<Card>();
		curr.addAll(onHand);
		curr.removeAll(curr_ranking);
		
		List<Card> oppo = new ArrayList<Card>();
		oppo.addAll(oppoHand);
		oppo.removeAll(oppo_ranking);
		
		Collections.sort(curr);
		Collections.sort(oppo);
		int dis = 0;
		for(int i=0;i<curr.size();i++)
		{
			Card card = curr.get(i);
			Card temp = oppo.get(i);
			dis = card.getIntRank() - temp.getIntRank();
			if(dis != 0)
			{
				break;
			}
		}	
		return dis;
	}
	
	/**
	 * generate the fifth card, and add them to the tree
	 * @param player
	 * @param opponent
	 */
	public void generateFifthCard(Player player, Player opponent)
	{
		List<Node> nodes = new ArrayList<Node>();
		nodes.addAll(secondTreeParentNodes);
		nodes.addAll(noSecondTreeNodes);
		
		List<Card> cards = new ArrayList<Card>();
		cards.addAll(deck.cards);
		cards.addAll(opponent.cards);
		
		for(Node node : nodes)					//leaf nodes which can generate trees in the first tree
		{			
			List<Node> children = new ArrayList<Node>();
			for(Card card : cards)				//46 different kinds of cards
			{
				Node temp = new Node(CHANCE_NODE,card, node, node.level+1);
				children.add(temp);							 
			}
			//add the chance node to leaf nodes children
			node.children = children;
		}		
	}	
	
	/**
	 * get the leaf nodes in the second tree
	 * @param node
	 */
	public void getLeafNodes(Node node)
	{
		if(node != null)
		{
			List<Node> children = node.children;
			if(children.isEmpty())
			{
				return;
			}else{
				for(Node temp : children)
				{
					if(temp.children.isEmpty() || temp.children==null)
					{
						leaf.add(temp);
					}														
				}	
				for(Node temp : children)
				{
					getLeafNodes(temp);
				}
			}
		}
	}
	
	/**
	 * get chance nodes
	 * @param player
	 * @param oppo
	 * @param parent
	 * @param chanceNum
	 */
	public void generateTreeNodes(int playerMoney, Node parent, int chanceNum, int belongs, Card fifthCard)
	{
		Node checkNode = new Node(CHECK, parent, playerMoney, chanceNum, belongs, fifthCard, parent.level+1);
		parent.children.add(checkNode);
		getNode(checkNode, chanceNum, belongs);
			
		if(playerMoney-250 >= 0)
		{
			Node bet250Node = new Node(BET250, parent, playerMoney-250, chanceNum, belongs, fifthCard, parent.level+1);
			parent.children.add(bet250Node);
			getNode(bet250Node, chanceNum, belongs);
		}
		
		Node allInNode = new Node(ALLIN, parent, 0, chanceNum, belongs, fifthCard, parent.level+1);
		parent.children.add(allInNode);					
		getNode(allInNode, chanceNum, belongs);
	}
	
	/**
	 * get second tree nodes
	 * @param belongs
	 */
	public void getSecondTreeNodes(int belongs)
	{
		//in second chance nodes, all of them are check and call nodes
		if(parentNode != null)
		{
			for(Node node : secondTreeParentNodes)
			{		
				for(Node child : node.children)
				{
					if(node.belongs==belongs)
					{
						generateTreeNodes(node.restMoney, child, 2, node.belongs, child.fifthCard);
					}else{
						generateTreeNodes(node.parent.restMoney, child, 2, node.parent.belongs, child.fifthCard);
					}
				}
			}
		}
	}
	
	/**
	 * check the highest sequence
	 * @param curr
	 * @param oppo
	 * @return bigger than 0 means current player has the highest sequence,
	 * 		equals to 0 means current player has the same highest sequence with opponent,
	 * 		smaller than 0 means opponent player has the highest sequence
	 */
	public int checkHighSequence(List<Card> curr, List<Card> oppo)
	{		
		int seq1 = 0;
		int seq2 = 0;
		for(Card card : curr)
		{
			seq1 += card.getIntRank();
		}
		for(Card card : oppo)
		{
			seq2 += card.getIntRank();
		}
		return seq1-seq2;
	}
	
	/**
	 * check the highest card
	 * @param curr
	 * @param oppo
	 * @return bigger than 0 means current player has the highest card,
	 * 		equals to 0 means current player has the same highest card with opponent,
	 * 		smaller than 0 means opponent player has the highest card
	 */
	public int checkHighCard(Card curr, Card oppo)
	{
		curr.setHigh();
		oppo.setHigh();
		
		int dis = curr.getIntRank() - oppo.getIntRank();	
		
		curr.setLow();
		oppo.setLow();
		
		return dis;
	}
	
	/**
	 * get node
	 * @param node
	 * @param chanceNode
	 * @param belongs
	 */
	public void getNode(Node node, int chanceNode, int belongs)
	{			
		belongs = 3-belongs;
			
		Node checkNode = null;		
		Node allInNode = null;		
		Node raiseDoubleNode = null;		
		Node callNode = null;
		Node foldNode = null;
		Node bet250Node = null;
		
		//if there are two connected checks, then end this bets
		if(CHECK.equals(node.name))
		{
			checkNode = new Node(CHECK, node, node.restMoney, chanceNode, belongs, node.fifthCard, node.level+1);
			node.children.add(checkNode);
			if(chanceNode==1)
				secondTreeParentNodes.add(checkNode);
						
			if(node.restMoney-250 >= 0)
			{
				bet250Node = new Node(BET250, node, node.restMoney-250, chanceNode, belongs, node.fifthCard, node.level+1);
				node.children.add(bet250Node);
				getNode(bet250Node, chanceNode, belongs);
			}
							
			allInNode = new Node(ALLIN, node,0, chanceNode, belongs, node.fifthCard, node.level+1);
			node.children.add(allInNode);
			getNode(allInNode, chanceNode, belongs);
		}else if(BET250.equals(node.name))
		{					
			callNode = new Node(CALL, node, node.restMoney, chanceNode, belongs, node.fifthCard, node.level+1);		
			node.children.add(callNode);
			getNode(callNode, chanceNode, belongs);
		
			//the total money that parent node bet	 
			int money = PLAYER_MONEY-node.restMoney;
			if(PLAYER_MONEY-money*2 >= 0)
			{
				raiseDoubleNode = new Node(RAISEDOUBLE, node, PLAYER_MONEY-money*2, chanceNode, belongs, node.fifthCard, node.level+1);
				node.children.add(raiseDoubleNode);
				getNode(raiseDoubleNode, chanceNode, belongs);
			}
			
			allInNode = new Node(ALLIN, node, 0, chanceNode, belongs, node.fifthCard, node.level+1);
			node.children.add(allInNode);
			getNode(allInNode, chanceNode, belongs);
			
			foldNode = new Node(FOLD, node, node.parent.restMoney, chanceNode, belongs, node.fifthCard, node.level+1);
			node.children.add(foldNode);
			
		}else if(ALLIN.equals(node.name))
		{
			allInNode = new Node(ALLIN, node, 0, chanceNode, belongs, node.fifthCard, node.level+1);
			node.children.add(allInNode);
			if(chanceNode==1)
			{
				noSecondTreeNodes.add(allInNode);
			}			
			
			foldNode = new Node(FOLD, node, node.parent.restMoney, chanceNode, belongs, node.fifthCard, node.level+1);
			node.children.add(foldNode);
			
		}else if(CALL.equals(node.name))
		{
			if(chanceNode==1)
			{
				//if the rest money is more than 0, then the node will
				//generate another tree
				if(node.restMoney==0)
				{
					noSecondTreeNodes.add(node);
				}else{
					secondTreeParentNodes.add(node);
				}
			}				
			return;
		}else if(RAISEDOUBLE.equals(node.name))
		{					
			callNode = new Node(CALL, node, node.restMoney, chanceNode, belongs, node.fifthCard, node.level+1);
			node.children.add(callNode);
			getNode(callNode, chanceNode, belongs);
			
			int money = PLAYER_MONEY-node.restMoney;
			if(PLAYER_MONEY-money*2 >= 0)
			{
				raiseDoubleNode = new Node(RAISEDOUBLE, node, PLAYER_MONEY-money*2, chanceNode, belongs, node.fifthCard, node.level+1);
				node.children.add(raiseDoubleNode);
				getNode(raiseDoubleNode, chanceNode, belongs);
			}
			
			allInNode = new Node(ALLIN, node, 0, chanceNode, belongs, node.fifthCard, node.level+1);
			node.children.add(allInNode);
			getNode(allInNode, chanceNode, belongs);
			
			foldNode = new Node(FOLD, node, node.parent.restMoney, chanceNode, belongs, node.fifthCard, node.level+1);
			node.children.add(foldNode);
		}else if(ALLIN.equals(node.name))
		{
			allInNode = new Node(ALLIN, node, 0, chanceNode, belongs, node.fifthCard, node.level+1);			
			node.children.add(allInNode);
			
			foldNode = new Node(FOLD, node, node.parent.restMoney, chanceNode, belongs, node.fifthCard, node.level+1);
			node.children.add(foldNode);
		}else if(FOLD.equals(node.name))
		{
			return;
		}		
	}	
}
