import java.util.ArrayList;
import java.util.List;


public class Node implements Comparable<Node>{
	
	public String name;
	public List<Node> children = new ArrayList<Node>();
	public Node parent;
	//1 is player1, 2 is player2
	public int belongs;
	public int restMoney;
	public int chanceNode;
	public double winnedMoney;	
	public Card fifthCard;
	public double minMoney;
	public double maxMoney;
	public int level;
	public int betMoney;
	
	public Node()
	{
		
	}
	
	public Node(String name, Card fifthCard, Node parent, int level)
	{
		this.name = name;
		this.fifthCard = fifthCard;
		this.parent = parent;
		this.level = level;
		this.restMoney = parent.restMoney;
	}
	
	public Node(String name, Node parent, int restMoney, int chanceNode, int belongs, int level) {
		this.name = name;		
		this.parent = parent;
		this.restMoney = restMoney;
		this.chanceNode = chanceNode;
		this.belongs = belongs;
		this.level = level;
	}
	
	public Node(String name, Node parent, int restMoney, int chanceNode, int belongs,
			Card fifthCard, int level) {
		this.name = name;		
		this.parent = parent;
		this.restMoney = restMoney;
		this.chanceNode = chanceNode;
		this.belongs = belongs;
		this.fifthCard = fifthCard;
		this.level = level;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Node> getChildren() {
		return children;
	}
	public void setChildren(List<Node> children) {
		this.children = children;
	}
	public Node getParent() {
		return parent;
	}
	public void setParent(Node parent) {
		this.parent = parent;
	}
	public int getRestMoney() {
		return restMoney;
	}
	public void setRestMoney(int restMoney) {
		this.restMoney = restMoney;
	}
	public int getBelongs() {
		return belongs;
	}
	public void setBelongs(int belongs) {
		this.belongs = belongs;
	}
	public int getChanceNode() {
		return chanceNode;
	}
	public void setChanceNode(int chanceNode) {
		this.chanceNode = chanceNode;
	}
	public double getWinnedMoney() {
		return winnedMoney;
	}
	public void setWinnedMoney(double winnedMoney) {
		this.winnedMoney = winnedMoney;
	}
	public Card getFifthCard() {
		return fifthCard;
	}

	public void setFifthCard(Card fifthCard) {
		this.fifthCard = fifthCard;
	}

	public double getMinMoney() {
		return minMoney;
	}

	public void setMinMoney(double minMoney) {
		this.minMoney = minMoney;
	}

	public double getMaxMoney() {
		return maxMoney;
	}

	public void setMaxMoney(double maxMoney) {
		this.maxMoney = maxMoney;
	}

	public int getBetMoney() {
		return betMoney;
	}

	public void setBetMoney(int betMoney) {
		this.betMoney = betMoney;
	}

	public String toString()
	{
		String result = "parent name: "+name+",  min money: "+minMoney+", max money: "+maxMoney+"\n";
		result += "children: \n";
		for(Node node : children)
		{
			result += "[name: "+node.name+",  min money: "+node.minMoney+
					", max money: "+node.maxMoney+", rest money: "+node.restMoney+"]\n";
		}
		return result;
	}

	@Override
	public int compareTo(Node o) {	
		return (int) (winnedMoney-o.winnedMoney);
	}
}
