import java.util.ArrayList;
import java.util.List;


public class Play {

	public static void main(String args[])
	{
		/**
		 * The first parameter will be an integer value, giving the number of hands to play
		 */
		int hands = Integer.parseInt(args[0]);
		/**
		 * The second parameter will be a flag,
		 * if flag is 'r', then it is rational,
		 * if flag is 'n', then it is non-rational.
		 */
		String rationalFlag = args[1];
		
		/**
		 * The third parameter will be an optional flag,
		 * if flag is 'v', then it is verbose, then the program will output full details of every hand
		 * if flag is not included, then the program will run all the hands and provide only a summary 
		 * of the results as it goes
		 */
		String optionalFlag = "";
		
		if(args.length == 3)
		{
			optionalFlag = args[2];
		}		
	
		if(optionalFlag.equals(""))
		{
			System.out.printf("%10s%20s%20s\n","Game","P1_Result","P2_Result");
		}
		boolean isPlayer1Rational = false;
		boolean isPlayer2Rational = false;
		if(rationalFlag.equals("r"))
		{
			isPlayer1Rational = true;
			isPlayer2Rational = true;
		}else{
			isPlayer1Rational = true;
			isPlayer2Rational = false;
		}
		
		GameTexasHoldem gth = new GameTexasHoldem(rationalFlag, optionalFlag);
		int handCount = 0;
		
		while(handCount < hands)
		{
			Player firstPlayer = null;
			Player secondPlayer = null;
			
			//generate a deck of cards
			Deck deck = new Deck();
			
			//generate the card of player
			List<Card> cards1 = new ArrayList<Card>();
			cards1.add(deck.pop());
			cards1.add(deck.pop());
			
			//generate the cards of player
			List<Card> cards2 = new ArrayList<Card>();
			cards2.add(deck.pop());
			cards2.add(deck.pop());
			
			Player player1 = new Player(1, 1500, cards1, isPlayer1Rational);
			Player player2 = new Player(2, 1500, cards2, isPlayer2Rational);
			
			if(handCount%2 == 0)
			{
				firstPlayer = player1;
				secondPlayer = player2;
			}else{
				firstPlayer = player2;
				secondPlayer = player1;
			}
			
			if(optionalFlag.equals("v"))
			{
				System.out.println("----------------");
				System.out.println(" Hand "+(handCount+1));
				System.out.println("----------------");
	
				System.out.print("Player"+firstPlayer.number+":  ");
				for(Card card : firstPlayer.cards)
				{
					System.out.print(card.toString());
				}
				System.out.println("$"+firstPlayer.money);				
				
				System.out.print("Player"+secondPlayer.number+":  ");
				for(Card card : secondPlayer.cards)
				{
					System.out.print(card.toString());
				}
				System.out.println("$"+secondPlayer.money);	
			}	
			gth.newGame(deck, firstPlayer, secondPlayer);
			//start the game
			gth.startGame();
			handCount++;
		}	
		//print all the result
		gth.printResult();
	}
}
