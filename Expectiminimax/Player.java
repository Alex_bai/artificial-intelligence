import java.util.List;


public class Player {

	public int number;
	public int money;
	public List<Card> cards;
	public Card highCard = null;
	public List<Card> rankingList = null; 
	public boolean isTurn; 	
	public RankingEnum rankingEnum;
	public boolean isRational;
	
	public Player()
	{
		
	}
	public Player(int number, int money, List<Card> cards, boolean isRational) {
		this.number = number;
		this.money = money;
		this.cards = cards;
		this.isRational = isRational;
	}
	
	public Player(int number, int money, List<Card> cards) {
		this.number = number;
		this.money = money;
		this.cards = cards;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public int getMoney() {
		return money;
	}
	public void setMoney(int money) {
		this.money = money;
	}
	
	public List<Card> getCards() {
		return cards;
	}
	public void setCards(List<Card> cards) {
		this.cards = cards;
	}
	public Card getHighCard() {
		return highCard;
	}
	public void setHighCard(Card highCard) {
		this.highCard = highCard;
	}
	public List<Card> getRankingList() {
		return rankingList;
	}
	public void setRankingList(List<Card> rankingList) {
		this.rankingList = rankingList;
	}
	public boolean isTurn() {
		return isTurn;
	}
	public void setTurn(boolean isTurn) {
		this.isTurn = isTurn;
	}	

	public RankingEnum getRankingEnum() {
		return rankingEnum;
	}

	public void setRankingEnum(RankingEnum rankingEnum) {
		this.rankingEnum = rankingEnum;
	}
	public boolean isRational() {
		return isRational;
	}
	public void setRational(boolean isRational) {
		this.isRational = isRational;
	}
	public String toString()
	{
		return number+" "+money;
	}	
	public Player clone()
	{
		Player temp = new Player();
		temp.setCards(this.getCards());
		temp.setHighCard(this.getHighCard());
		temp.setMoney(this.getMoney());
		temp.setNumber(this.getNumber());
		temp.setRankingList(this.getRankingList());
		temp.setTurn(this.isTurn());
		return temp;
	}
}
