import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class MarkovDecisionProcess {

	public int row;
	public int col;
	public int locations[][]; 
	public Location start;
	public State startState;
	public List<Location> goals;
	public int capacity;	
	public int updateNum;	
	public List<State> states; 
	public double avg;
	public final int ACTION_UP = 0;
	public final int ACTION_LEFT = 1;
	public final int ACTION_DOWN = 2;
	public final int ACTION_RIGHT = 3;
	public final int ACTION_SIZE = 4;
	public final double theta = 0.01; 
	public final int WALL = -1;	
	public final double SUCCESS = 0.7;
	public final double OTHER_PRO = 0.1;
	public final int GOAL_VALUE = 10000;
	public final int TRIALS = 500;
	public final double gamma = 0.9;
	
	public MarkovDecisionProcess(String fileName, int capacity)
	{
		this.goals = new ArrayList<Location>();
		this.states = new ArrayList<State>();
		this.capacity = capacity;		
	
		readFile(fileName);		
	}
	
	/**
	 * read the data from file
	 * @param fileName
	 */
	public void readFile(String fileName)
	{		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File(fileName)));
			String result= "";
			String data = "";
			while((data=reader.readLine())!=null)
			{
				result += data+"\n";
			}
			
			Scanner scanner = new Scanner(result);
			row = scanner.nextInt();
			col = scanner.nextInt();
			
			locations = new int[row][col];
			for(int i=0;i<row;i++)
			{				
				for(int j=0;j<col;j++)
				{
					int value = scanner.nextInt();
					
					if(value==1)
					{
						//initial the start location
						start = new Location(i, j, value);
					}else if(value >1)
					{
						//get the goal locations
						goals.add(new Location(i, j, value));
					}
					locations[i][j] = value;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * get the carried locations
	 */
	public void getCarriedLocation()
	{
		//sort the goals list on the order of desc
		Collections.sort(goals);
		//get ride of the locations that out of the capacity
		List<Location> restGoals = new ArrayList<Location>();
		if(capacity < goals.size())
		{
			for(int i=0;i<capacity;i++)
			{
				restGoals.add(goals.get(i));
			}	
			goals = restGoals;
		}else{
			capacity = goals.size();
		}
		
	}
	
	/**
	 * generate all the states in the maze
	 */
	public void generateStates()
	{
		for(int i=0;i<row;i++)
		{
			for(int j=0;j<col;j++)
			{
				if(locations[i][j] != -1)
				{
					State state = new State(i, j, 0);
					states.add(state);
					for(int m=1;m<=capacity;m++)
					{
						State temp = new State(i, j, 0);
						getAllKinds(i, j, m, temp, null);
					}
				}
			}
		}
	}	
	
	/**
	 * generate all kinds of states 
	 * @param row
	 * @param col
	 * @param m
	 */
	public void getAllKinds(int row, int col, int m, State state, Location location)
	{	
		State tempState = new State(state.row, state.col, state.utility);
		tempState.goalsVisited.addAll(state.goalsVisited);
		
		if(location != null)
		{
			tempState.getGoals().add(location);
		}
		if(m==0)
		{
			if(!states.contains(tempState))
			{
				states.add(tempState);
			}
			return;
		}
		
		for(int i=0;i<goals.size();i++)
		{
			Location loc = goals.get(i);
			if(!tempState.goalsVisited.contains(loc))
			{
				getAllKinds(row, col, m-1, tempState, loc);
			}	
		}
	}
	
	/**
	 * find the start state from states
	 */
	public State findState(int r, int c, List<Location> carried)
	{		
		if(locations[r][c] == -1)
		{
			return null;
		}
		for(State state : states)
		{
			if(state.row == r && state.col == c && state.goalsVisited(carried))
			{
				return state;				
			}
		}
		return null;
	}
	
	/**
	 * generate random policy
	 */
	public void generateRandomPolicy()
	{
		for(State state : states)
		{
			state.action = (int) (Math.random()*ACTION_SIZE);
		}
		for(State state : states)
		{
			state.next = getNextState(state);
		}
	}
	
	/**
	 * find the next state from states
	 * @param state
	 * @return
	 */
	public State getNextState(State state)
	{
		int action = state.action;
		int r = state.row;
		int c = state.col;
		switch(action)
		{
			case ACTION_UP:
				r--;
				break;
			case ACTION_LEFT:
				c--;
				break;
			case ACTION_DOWN:
				r++;
				break;
			case ACTION_RIGHT:
				c++;
				break;
		}			
		
		//if it is out of boundary
		if(r<0 || r>=row || c<0 || c>=col)
		{
			return state;
		}
		//get next location value
		int locValue = locations[r][c];
		if(locValue < 0)
		{
			return state;
		}
			
		//List<Location> carried = state.goals;
		List<Location> goalsVisited = new ArrayList<Location>();
		goalsVisited.addAll(state.goalsVisited);
		
		for(Location loc : goalsVisited)
		{
			//if next location is goal
			if(loc.row==r && loc.col==c)
			{
				return findState(r, c, goalsVisited);
			}
		}
				
		if(locValue > 1)
		{
			goalsVisited.add(new Location(r, c, locValue));	
		}
		return findState(r, c, goalsVisited);
	}
	
	/**
	 * calculate the policy for all the states
	 */
	public void policyEvaluation()
	{
		double delta = 10;
		while(delta >= theta)
		{
			delta = 0;
			for(State s : states)
			{
				double utility = s.utility;
				State next = s.next;
				if(next==null)
				{
					s.utility = WALL + gamma*s.utility;
				}else if(isGoal(s))
				{
					if(s.goalsVisited.size()==capacity)
					{
						s.utility = GOAL_VALUE;
					}else{
						s.utility = locations[next.row][next.col] + gamma*next.utility;
					}				
				}else{
					s.utility = WALL + gamma*next.utility;
				}
				delta = Math.max(delta, Math.abs(s.utility-utility));
			}
		}		
	}
	
	/**
	 * calculate the policy for all the states based on the non-deterministic
	 */
	public void policyEvaluationForNonDetermin()
	{
		double delta = 10;
		while(delta >= theta)
		{			
			delta = 0;
			for(State s : states)
			{
				double utility = s.utility;
				State next = s.next;
				//get the utility that is optimal
				if(next==null)
				{
					s.utility = SUCCESS * (WALL + gamma*s.utility);
				}else if(isGoal(s))
				{
					if(s.goalsVisited.size()==capacity)
					{
						s.utility = SUCCESS * GOAL_VALUE;
					}else{
						s.utility = SUCCESS * 
								(locations[next.row][next.col] + gamma*next.utility);
					}				
				}else{
					s.utility = SUCCESS * (WALL + gamma*next.utility);
				}
				
				//add the rest states' utility
				List<State> otherStates = getOtherStates(s);
				for(State state : otherStates)
				{
					if(s.goalsVisited.size() != state.goalsVisited.size())
					{
						s.utility += OTHER_PRO * 
								(locations[state.row][state.col] + gamma*state.utility);
					}else{
						if(state.goalsVisited.size()==capacity && isGoal(state))
						{
							s.utility += OTHER_PRO * GOAL_VALUE;						
						}else{
							s.utility += OTHER_PRO * (WALL + gamma*state.utility);
						}
					}
				}			
				delta = Math.max(delta, Math.abs(s.utility-utility));
			}
		}
	}
	
	/**
	 * iterate the policy for the deterministic
	 */
	public void policyIteration()
	{
		boolean changed = true;
		while(changed)
		{
			updateNum++;
			
			policyEvaluation();	
			
			changed = false;
			
			for(State s : states)
			{			
				int action = s.action;
				double maxUtility = Double.MIN_VALUE;
				
				for(int j=0;j<ACTION_SIZE;j++)
				{
					State temp = s.clone();
					temp.action = j;			
					State next = getNextState(temp);
					if(next != null)
					{
						if(next.utility > maxUtility)
						{
							action = temp.action;
							s.next = next;
							maxUtility = next.utility;
						}
					}
				}
				
				if(s.action != action)
				{
					changed = true;
				}
				s.setAction(action);				
			}
		}
	}
	
	/**
	 * iterate the policy for the non deterministic
	 */
	public void policyIterationForNonDetermin()
	{		
		boolean changed = true;
		while(changed)
		{
			updateNum++;
			
			policyEvaluationForNonDetermin();
			
			changed = false;
			
			for(State s : states)
			{			
				int action = s.action;
				double maxUtility = Double.MIN_VALUE;
				
				for(int j=0;j<ACTION_SIZE;j++)
				{
					State temp = s.clone();
					temp.action = j;			
					State next = getNextState(temp);
					if(next != null)
					{
						if(next.utility > maxUtility)
						{
							action = temp.action;
							s.next = next;
							maxUtility = next.utility;
						}
					}
				}
				
				if(s.action != action)
				{
					changed = true;
				}
				s.setAction(action);				
			}
		}
	}	
	
	/**
	 * get other states in the other actions
	 * @param s
	 * @return
	 */
	public List<State> getOtherStates(State s)
	{
		List<State> other = new ArrayList<State>();
		for(int i=0;i<ACTION_SIZE;i++)
		{
			if(s.originAction!=i)
			{
				State temp = s.clone();
				temp.setAction(i);
				temp.next = this.getNextState(temp);
				if(temp.next != null)
				{
					other.add(temp.next);
				}
			}else{
				avg-=0.05;
			}
		}
		return other;
	}
	
	/**
	 * judge whether the state s is the goal
	 * @param s
	 * @return
	 */
	public boolean isGoal(State s)
	{
		for(Location loc : goals)
		{
			if(s.row == loc.row && s.col == loc.col)
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * print deterministic result
	 */
	public void printDeterministicResult()
	{
		System.out.println("Testing Deterministic MDP.\n");
		System.out.println("Target values to obtain: "+goals.toString()+"\n");
		System.out.println("Doing policy iteration...");
		System.out.println("Evaluated and updated "+updateNum+" policies.");
		System.out.println("Done.");
		
		updateNum = 0;
		
		State state = startState;
		List<Integer> actions = new ArrayList<Integer>();
		actions.add(state.action);
		while(state.goalsVisited.size() != capacity)
		{
			state = state.next;
			actions.add(state.action);
		}
		System.out.print("Path of Length "+(actions.size()-1)+": [");
		for(int i=0;i<actions.size()-2;i++)
		{
			int action = actions.get(i);
			String resAction = action2String(action);
			System.out.print(resAction+", ");
		}
		String resAction = action2String(actions.get(actions.size()-2));
		System.out.print(resAction+"]");
	}
	
	/**
	 * calculate 500 trials
	 */
	public void NonDeterminPlay()
	{		
		for(int i=0;i<TRIALS;i++)
		{
			State state = this.startState;
			
			while(state.goalsVisited.size() != capacity)
			{																
				double pro = Math.random();
				if(pro < SUCCESS)
				{
					state = state.next;					
				}else{
					List<State> otherStates = this.getOtherStates(state);
					state = otherStates.get((int)(Math.random()*otherStates.size()));					
				}
				avg++;
			}
		}
		avg /= TRIALS;
	}
	
	/**
	 * print non deterministic result
	 */
	public void pringNonDeterministicResult()
	{
		System.out.println("Testing Stochastic MDP.\n");
		System.out.println("Target values to obtain: "+goals.toString()+"\n");
		System.out.println("Doing policy iteration...");
		System.out.println("Evaluated and updated "+updateNum+" policies.");
		System.out.println("Done.\n");		
		System.out.println("Average path length to pick up "+capacity+" objects: "+String.format("%.2f", avg));		
	}
	
	/**
	 * change int value to string value for action
	 * @param action
	 * @return
	 */
	public String action2String(int action)
	{
		String resAction = "";
		switch(action)
		{
			case ACTION_UP:
				resAction = "Up";
				break;
			case ACTION_LEFT:
				resAction = "Left";
				break;
			case ACTION_DOWN:
				resAction = "Down";
				break;
			case ACTION_RIGHT:
				resAction = "Right";
				break;
		}
		return resAction;
	}
	
	/**
	 * clean all the states
	 */
	public void cleanStates()
	{
		for(State state : states)
		{
			state.utility = 0;
		}
	}	
}









