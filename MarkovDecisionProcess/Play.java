
import java.util.ArrayList;
import java.util.List;

public class Play {

	public static MarkovDecisionProcess mdp;
	
	public static void main(String args[])
	{		
		String fileName = args[0];
		int capacity = Integer.parseInt(args[1]);
		
		mdp = new MarkovDecisionProcess(fileName, capacity);
				
		deterministicPlay();
		
		System.out.println("\n\n*******************************\n");
		
		nonDeterministicPlay();

	}	
	
	public static void deterministicPlay()
	{
		//get carried locations
		mdp.getCarriedLocation();
		//generate all the states
		mdp.generateStates();
		//find start state from states
		Location start = mdp.start;
		mdp.startState = mdp.findState(start.row, start.col, new ArrayList<Location>());	
		//generate random policy
		mdp.generateRandomPolicy();
		//improve the policy
		mdp.policyIteration();
		//print result
		mdp.printDeterministicResult();
	}
	
	public static void nonDeterministicPlay()
	{
		//clean all the states
		mdp.cleanStates();						
		//improve the policy		
		mdp.policyIterationForNonDetermin();
		
		mdp.NonDeterminPlay();
		//print result
		mdp.pringNonDeterministicResult();
	}
}
