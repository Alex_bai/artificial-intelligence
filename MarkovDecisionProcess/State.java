import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class State implements Cloneable{

	public int row;
	public int col;
	public int action;
	public int originAction;
	public List<Location> goalsVisited;
	public double utility;
	public State next;
	
	public State(int row, int col, double utility)
	{
		this.row = row;
		this.col = col;
		this.goalsVisited = new ArrayList<Location>();		
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getCol() {
		return col;
	}

	public void setCol(int col) {
		this.col = col;
	}

	public int getAction() {
		return action;
	}

	public void setAction(int action) {
		this.action = action;
		this.originAction = action;
	}

	public List<Location> getGoals() {
		return goalsVisited;
	}

	public void setGoals(List<Location> goals) {
		this.goalsVisited = goals;
	}

	public double getUtility() {
		return utility;
	}

	public void setUtility(double utility) {
		this.utility = utility;
	}

	public State getNext() {
		return next;
	}

	public void setNext(State next) {
		this.next = next;
	}	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + action;
		result = prime * result + col;
		result = prime * result
				+ ((goalsVisited == null) ? 0 : goalsVisited.hashCode());
		result = prime * result + ((next == null) ? 0 : next.hashCode());
		result = prime * result + originAction;
		result = prime * result + row;
		long temp;
		temp = Double.doubleToLongBits(utility);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		State other = (State) obj;
		if (action != other.action)
			return false;
		if (col != other.col)
			return false;
		if (goalsVisited == null) {
			if (other.goalsVisited != null)
				return false;
		} else if (!goalsVisited.equals(other.goalsVisited))
			return false;
		if (next == null) {
			if (other.next != null)
				return false;
		} else if (!next.equals(other.next))
			return false;
		if (originAction != other.originAction)
			return false;
		if (row != other.row)
			return false;
		if (Double.doubleToLongBits(utility) != Double
				.doubleToLongBits(other.utility))
			return false;
		return true;
	}

	/**
	 * compare the two list
	 * @param otherGoals
	 * @return
	 */
	public boolean goalsVisited(List<Location> otherGoals)
	{
		Collections.sort(goalsVisited);
		Collections.sort(otherGoals);
		
		if(goalsVisited.size() == otherGoals.size())
		{
			for(int i=0;i<goalsVisited.size();i++)
			{
				Location loc = goalsVisited.get(i);
				Location other = otherGoals.get(i);
				if(!loc.equals(other))
				{
					return false;
				}
			}
		}else{
			return false;
		}		
		return true;
	}
	
	public State clone()
	{
		State state = null;
		state = new State(this.row, this.col, this.utility);
		state.action = this.action;
		state.goalsVisited.addAll(this.goalsVisited);
		return state;
	}
	
	public String toString()
	{
		return row+" "+col;
	}
}
