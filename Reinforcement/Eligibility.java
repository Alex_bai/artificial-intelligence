
public class Eligibility implements Comparable<Eligibility>{
	public State state; 
	public int action;
	public double value;
	
	public Eligibility(State state, int action, double value) {		
		this.state = state;
		this.action = action;		
		this.value = value;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public int getAction() {
		return action;
	}

	public void setAction(int action) {
		this.action = action;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + action;
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Eligibility other = (Eligibility) obj;
		if (action != other.action)
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		return true;
	}

	@Override
	public int compareTo(Eligibility o) {
		// TODO Auto-generated method stub
		return new Double(this.value).compareTo(new Double(o.value));
	}		
	
	public String toString()
	{
		return "["+state.row+","+state.col+","+action+","+value+"]";
	}
}
