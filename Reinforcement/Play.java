

public class Play {
	
	public static String BestPolicyFileName = "BestPolicy.txt";
	public static String QLearningPolicyFileName = "QLearningPolicy.txt";
	public static String SARSALambdaFileName = "SARSALambda.txt";
	
	public static String totalRewardsFileName = "Rewards.csv";
	public static String RewardsGraphFileName = "RewardsGraph.csv";
	
	public static void main(String args[])
	{			
		String fileName = args[0];
		int reduction = Integer.parseInt(args[1]);
		
		ReinforcementLearning mdp = new ReinforcementLearning
				 (fileName, BestPolicyFileName, reduction);
				
		//SARSA Learning
		System.out.println("SARSA Learning");
		mdp.SARSALearning();
		mdp.clean();
	
		//Q Learning
		System.out.println("Q Learning");
		mdp.QLearning();		
		mdp.clean();
				
		//SARSA Lambda Learning
		System.out.println("SARSA Lambda Learning");
		mdp.SARSALambda();
		
		//generate best policy
		mdp.generatePolicyFile();
		
		//generate reward 
		mdp.generateRewardFile(totalRewardsFileName);
		
		//generate reward to get the graph
		mdp.generateRewardGraphFile(RewardsGraphFileName);
	}	
}
