Some easy steps to run program
1. Compile
	javac *.java
2. Run
	java Play iceWorld_larger.txt 800

The first parameter is file name, and the second parameter is the reduction value, you can input 800, 2000 and 5000.

the output file name has already in the code, and the output file will 
be stored in the current path.

the BestPolicy.txt file includes 3 best policies for SARSA Learning, Q Learning and SARSA Lambda Learning in the reduction.

the Rewards.cv file includes all the rewards for SARSA Learning, Q Learning and SARSA Lambda Learning in the reduction.

the RewardsGraph.csv includes all rewards to generate graph for SARSA Learning, Q Learning and SARSA Lambda Learning in the reduction.