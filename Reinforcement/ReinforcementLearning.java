import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Scanner;

public class ReinforcementLearning {

	public String[][] iceWorld;
	public Map<QValue, Double> Q;
	public LinkedList<Eligibility> propagated;
	public List<State> states; 
	public State startState;
	public State goalState;
	public String bestPolicy;
	public String policyName;
	public int SARSARewards[];
	public int QRewards[];
	public int SARSALambdaRewards[];
	public int episode;
	public int reducedValue;
	public int bunchSize;
		
	//constant
	public final float THETA_CONSTANT = 0.9f;	
	public final double GAMMA = 0.9;
	public final double ALPHA = 0.9;
	public final double LAMBDA = 0.9;	
	public final int PROPAGATED_SIZE = 10;		
	//reward	
	public final int HOLE_REWARD = -100;
	public final int NORMAL_REWARD = -1;
	public final int GOAL_REWARD = 0;
	//action value
	public final int ACTION_UP = 0;
	public final int ACTION_LEFT = 1;
	public final int ACTION_DOWN = 2;
	public final int ACTION_RIGHT = 3;
	public final int ACTION_SIZE = 4;
	
	public ReinforcementLearning(String fileName, String policyName, int episode)
	{		
		this.states = new ArrayList<State>();	
		this.Q = new HashMap<QValue, Double>();
		this.bestPolicy = "";	
		this.policyName = policyName;
		this.episode = episode;
		this.SARSARewards = new int[episode];
		this.QRewards = new int[episode];	
		this.SARSALambdaRewards = new int[episode];
		this.bunchSize = episode/100;
		if(episode==2000)
		{
			this.reducedValue = 10;
		}else if(episode==5000)
		{
			this.reducedValue = 25;
		}else if(episode==800)
		{
			this.reducedValue = 4;
		}
		readFile(fileName);		
	}
	
	/**
	 * read the data from file
	 * @param fileName
	 */
	public void readFile(String fileName)
	{		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File(fileName)));
			String result= "";
			String data = reader.readLine();				
			//get the column value
			int col = data.length();
			int row = 0;
			while(data != null)
			{
				row++;
				result += data+"\n";
				data = reader.readLine();
			}			
			Scanner scanner = new Scanner(result);		
			iceWorld = new String[row][col];
			for(int i=0;i<row;i++)
			{				
				String value = scanner.next();
				iceWorld[i] = value.split("");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * generate all the states
	 */
	public void generateStates()
	{
		for(int i=0;i<iceWorld.length;i++)
		{
			for(int j=0;j<iceWorld[i].length;j++)
			{
				String value = iceWorld[i][j];
				if(value.equals("S"))
				{
					startState = new State(i, j, 0);
					states.add(startState);
				}else if(value.equals("G"))
				{
					goalState = new State(i, j, 0);
					states.add(goalState);
				}else{
					State state = new State(i, j, 0);
					states.add(state);
				}				
			}
		}
	}	
	
	/**
	 * initialize q
	 */
	public void initialQ()
	{		
		for(State state : states)
		{
			for(int a=0;a<ACTION_SIZE;a++)
			{
				QValue value = new QValue(state, a);
				Q.put(value, 0.0);
			}			
		}
	}
	
	/**
	 * get theta
	 * @param i
	 * @return
	 */
	public float getTheta(int i)
	{
		float theta = THETA_CONSTANT;
		if(i<=(reducedValue*100))
		{				
			if(i/reducedValue != 0)
			{
				theta /= i/reducedValue;
			}							
		}else{
			theta = 0;
		}	
		return theta;
	}
	
	/**
	 * SARSA Learning
	 */
	public void SARSALearning()
	{					
		//generate states
		generateStates();
		//initial q
		initialQ();
		
		for(int i=1;i<=episode;i++)
		{		
			System.out.println(i);
			int totalReward = 0;
			//get the theta
			float theta = getTheta(i);
			State state = startState;
			int action = greedily(state, theta);
			state.action = action;
			int stepNum = 0;
			while(!isGoal(state))
			{				
				if(theta < 0.1 && stepNum >= 3*states.size())
				{
					break;
				}
				State nextState = null;
				//take action and get next state
				switch(action)
				{
					case ACTION_UP:
						nextState = findState(state, state.row-1, state.col, action);
						break;
					case ACTION_LEFT:
						nextState = findState(state, state.row, state.col-1, action);
						break;
					case ACTION_DOWN:
						nextState = findState(state, state.row+1, state.col, action);
						break;
					case ACTION_RIGHT:
						nextState = findState(state, state.row, state.col+1, action);
						break;
				}			
				//one-step reward
				double reward = calculateReward(nextState.row, nextState.col);
				//add to totalReward
				totalReward += reward;
				//if the next state is 'H', then go back
				if(iceWorld[nextState.row][nextState.col].equals("H"))
				{				
					nextState = state;
				}			
				//set next action
				int nextAction = greedily(nextState, theta);				
				//calculate Q value
				calculateQValue(state, nextState, action, nextAction, reward);
				
				state = nextState;
				action = nextAction;
				state.action = action;								
				
				stepNum++;
			}
			//generate total reward
			SARSARewards[i-1] = totalReward;
			
		}
		bestPolicy += "\nSARSA Learning:\n\n";				
		bestPolicy += generateBestPolicy();
	}
	
	/**
	 * Q Learning
	 */
	public void QLearning()
	{			
		for(int i=1;i<=episode;i++)
		{
			int totalReward = 0;
			//get the theta
			float theta = getTheta(i);			
			State state = startState;
			int stepNum = 0;
			while(!isGoal(state))
			{
				if(theta < 0.1 && stepNum >= 3*states.size())
				{
					break;
				}
				int action = this.greedily(state, theta);
				state.action = action;
				
				State nextState = null;
				//take action and get next state
				switch(action)
				{
					case ACTION_UP:
						nextState = findState(state, state.row-1, state.col, action);
						break;
					case ACTION_LEFT:
						nextState = findState(state, state.row, state.col-1, action);
						break;
					case ACTION_DOWN:
						nextState = findState(state, state.row+1, state.col, action);
						break;
					case ACTION_RIGHT:
						nextState = findState(state, state.row, state.col+1, action);
						break;
				}				
				//one-step reward
				double reward = calculateReward(nextState.row, nextState.col);				
				totalReward += reward;
				//if the next state is 'H', then go back
				if(iceWorld[nextState.row][nextState.col].equals("H"))
				{
					nextState = state;
				}			
				//calculate q value for q learning
				calculateQValueForQLearning(state, nextState, action, reward);			
				
				state = nextState;
				stepNum++;
			}
			
			QRewards[i-1] = totalReward;
				
		}	
		bestPolicy += "\n\nQ Learning: \n\n";				
		bestPolicy += generateBestPolicy();					
	}

	/**
	 * get the index corresponding to the maximum value
	 * @param state
	 * @return
	 */
	public int getMaximumQValueIndex(State state)
	{
		//find maximum q
		double qValues[] = new double[ACTION_SIZE];	
		for(int a=0;a<ACTION_SIZE;a++)
		{
			qValues[a] = Q.get(findQValue(state, a));			
		}
				
		//find the biggest value
		double maxValue = -Double.MAX_VALUE;
		int index = -1;
		for(int i=0;i<qValues.length;i++)
		{
			if(qValues[i] > maxValue)
			{
				maxValue = qValues[i];
				index = i;
			}
		}
		return index;
	}
	
	/**
	 * sarsa lambda
	 */
	public void SARSALambda()
	{	
		for(int i=1;i<=episode;i++)
		{				
			System.out.println(i);
			int totalReward = 0;
			this.propagated = new LinkedList<Eligibility>();
			float theta = getTheta(i);			
			
			State state = startState;
			int action = getMaximumQValueIndex(state);			
			state.action = action;						
			
			int stepNum = 0;
			while(!isGoal(state))
			{						
				if(theta < 0.1 && stepNum >= 3*states.size())
				{
					break;
				}				
				State nextState = null;				
			
				//take action and get next state
				switch(action)
				{
					case ACTION_UP:
						nextState = findState(state, state.row-1, state.col, action);
						break;
					case ACTION_LEFT:
						nextState = findState(state, state.row, state.col-1, action);
						break;
					case ACTION_DOWN:
						nextState = findState(state, state.row+1, state.col, action);
						break;
					case ACTION_RIGHT:
						nextState = findState(state, state.row, state.col+1, action);
						break;
				}				
				//one-step reward
				double reward = calculateReward(nextState.row, nextState.col);
				//add to totalReward
				totalReward += reward;
				//if the next state is 'H', then go back
				if(iceWorld[nextState.row][nextState.col].equals("H"))
				{
					nextState = state;
				}			
				//set next action
				int nextAction = greedily(nextState, theta);			
				
				//calculate queue of the state with action
				Collections.sort(propagated);
				Eligibility eligibility = new Eligibility(state, action, 1);	
				if(propagated.contains(eligibility))
				{
					for(Eligibility e : propagated)
					{
						if(e.equals(eligibility))
						{
							eligibility.value += e.value;
							break;
						}
					}
					propagated.remove(eligibility);
					propagated.addLast(eligibility);
				}else{
					if(propagated.size()==PROPAGATED_SIZE)
					{
						propagated.removeFirst();
					}
					propagated.addLast(eligibility);
				}		
				//calculate formula
				calculateLambdaFormula(state, nextState, action, nextAction, reward);
				
				state = nextState;
				action = nextAction;
				state.action = action;
				
				stepNum++;
			}
			//generate total reward
			SARSALambdaRewards[i-1] = totalReward;
						
		}
		
		bestPolicy += "\n\nSARSA Lambda Learning:\n\n";				
		bestPolicy += generateBestPolicy();
	}
	
	/**
	 * generate policy file
	 */
	public void generatePolicyFile()
	{		
		generateFile(bestPolicy, policyName);
	}
	
	/**
	 * calculate e value
	 * @param index
	 * @param eligibility
	 * @return
	 */
	public double calEValue(int index, Eligibility eligibility)
	{
		if(index<0)
		{
			return 0;
		}else{
			Eligibility temp = propagated.get(index);
			if(temp.equals(eligibility))
			{				
				return GAMMA * LAMBDA * calEValue(index-1, eligibility) + 1;
			}else{
				return GAMMA * LAMBDA * calEValue(index-1, eligibility);
			}
		}		
	}
	
	/**
	 * get greedily action
	 * @param state
	 * @param theta
	 * @return
	 */
	public int greedily(State state, float theta)
	{
		double qValues[] = new double[ACTION_SIZE];
		
		for(int a=0;a<ACTION_SIZE;a++)
		{
			QValue qValue = findQValue(state, a);
			qValues[a] = Q.get(qValue);			
		}
		int action = -1;
		//theta ==0 ==> there is no random action
		if(theta == 0)
		{
			double maxValue = -Double.MAX_VALUE;	
			for(int i=0;i<qValues.length;i++)
			{
				double value = qValues[i];
				if(value > maxValue)
				{
					maxValue = value;
					action = i;
				}
			}
		}else{
			double rValue = Math.random();
			//rValue lte theta ==> choose the action at random
			if(rValue <= theta)
			{
				action = (int)(Math.random()*ACTION_SIZE);
			}else{
				//rValue gt theta ==> acting in a greedy fashion
				//get corresponding index which has the maximum value
				double maxValue = -Double.MAX_VALUE;	
				for(int i=0;i<qValues.length;i++)
				{
					double value = qValues[i];
					if(value > maxValue)
					{						
						maxValue = value;
						action = i;
					}
				}
			}
		}	
		return action;
	}
	
	/**
	 * find the state
	 * @param state
	 * @param row
	 * @param col
	 * @param action
	 * @return
	 */
	public State findState(State state, int row, int col, int action)
	{		
		if(row<0 || col<0 || row>=iceWorld.length || col>=iceWorld[0].length)
		{
			return state;
		}		
		if(iceWorld[row][col].equals("I"))
		{			
			Double value = Math.random();	
			if(value<0.1)
			{
				switch(action)
				{
					case ACTION_UP:						
						col--;
						break;
					case ACTION_LEFT:
						row++;						
						break;
					case ACTION_DOWN:						
						col++;
						break;
					case ACTION_RIGHT:
						row--;						
						break;
				}
			}else if(value>=0.1 && value<0.2)
			{
				switch(action)
				{
					case ACTION_UP:
						col++;
						break;
					case ACTION_LEFT:
						row--;
						break;
					case ACTION_DOWN:
						col--;
						break;
					case ACTION_RIGHT:
						row++;
						break;
				}
			}			
		}
		for(State s : states)
		{
			if(s.row==row && s.col==col)
			{					
				return s;
			}
		}
		return state;
	}
	
	/**
	 * calculate reward
	 * @param row
	 * @param col
	 * @return
	 */
	public double calculateReward(int row, int col)
	{
		String value = iceWorld[row][col];
		double reward = 0;
		switch(value)
		{
			case "H":
				reward = HOLE_REWARD;
				break;
			case "I":										
			case "O":
			case "S":
				reward = NORMAL_REWARD;
				break;
			default:
				reward = GOAL_REWARD;
				break;
		}
		return reward;
	}

	/**
	 * calculate q value for SARSA learning
	 * @param state
	 * @param nextState
	 * @param action
	 * @param nextAction
	 * @param reward
	 */
	public void calculateQValue(State state, State nextState, int action, int nextAction,
			double reward)
	{		
		double qValue = Q.get(findQValue(state, action));
		double nextQValue = Q.get(findQValue(nextState, nextAction));
		qValue += ALPHA * (reward + GAMMA*nextQValue-qValue);
		
		Q.put(findQValue(state, action), qValue);
	}

	/**
	 * calculate q value for Q learning
	 * @param state
	 * @param nextState
	 * @param action
	 * @param reward
	 */
	public void calculateQValueForQLearning(State state, State nextState, int action, double reward)
	{
		double qValue = Q.get(findQValue(state, action));
		
		//find maximum q
		double qValues[] = new double[ACTION_SIZE];
		for(int a=0;a<ACTION_SIZE;a++)
		{
			qValues[a] = Q.get(findQValue(nextState, a));			
		}
		
		//find the biggest value
		Arrays.sort(qValues);
		double nextQValue = qValues[qValues.length-1];
		
		qValue += ALPHA*(reward + GAMMA*nextQValue - qValue);
		
		Q.put(findQValue(state, action), qValue);
	}
	
	/**
	 * update q value and e value
	 * @param state
	 * @param nextState
	 * @param action
	 * @param nextAction
	 * @param reward
	 */
	public void calculateLambdaFormula(State state, State nextState, int action, int nextAction, double reward)
	{	
		double delta = reward + GAMMA*Q.get(this.findQValue(nextState, nextAction)) 
							- Q.get(this.findQValue(state, action));
						
		for(Eligibility e : propagated)
		{
			QValue qValue = this.findQValue(e.state, e.action);				
			
			Q.put(qValue, Q.get(qValue) + ALPHA*delta*e.value);
			e.value = GAMMA*LAMBDA*e.value;	
		}
	}	
	
	/**
	 * find the corresponding qValue based on the state and action
	 * @param state
	 * @param action
	 * @return
	 */
	public QValue findQValue(State state, int action)
	{
		for(QValue qValue : Q.keySet())
		{
			if(qValue.state.row == state.row && qValue.state.col == state.col 
					&& qValue.action==action)
			{
				return qValue;
			}
		}
		return null;
	}
	
	/**
	 * judge whether the state is goal state
	 * @param state
	 * @return
	 */
	public boolean isGoal(State state)
	{
		if(state.row==goalState.row && state.col==goalState.col)
		{
			return true;
		}
		return false;
	}
	
	/**
	 * generate the best policy
	 * @return
	 */
	public String generateBestPolicy()
	{
		String data = "";
		String result[][] = new String[iceWorld.length][iceWorld[0].length];		
		for(State state : states)
		{
			int row = state.row;
			int col = state.col;			
			
			String action = "";
			
			if(iceWorld[row][col].equals("H"))
			{
				result[row][col] = "H";
			}else{	
				int actionValue = getMaximumQValueIndex(state);								
				
				switch(actionValue)
				{
					case ACTION_UP:
						action = "U";
						break;
					case ACTION_LEFT:
						action = "L";
						break;
					case ACTION_DOWN:
						action = "D";
						break;
					case ACTION_RIGHT:
						action = "R";
						break;
				}
				result[row][col] = action;			
			}
		}	
			
		for(int i=0;i<result.length;i++)
		{
			for(int j=0;j<result[i].length;j++)
			{				
				if(iceWorld[i][j].equals("S"))
				{
					result[i][j] = "S";
				}								
				data += result[i][j];
			}	
			data += "\n";
		}
		return data;
	}
	
	/**
	 * generate file
	 * @param data
	 * @param fileName
	 */
	public void generateFile(String data, String fileName)
	{
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(new File(fileName)));			
			bw.write(data);
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * generate reward file for two algorithms
	 * @param fileName
	 */
	public void generateRewardFile(String fileName)
	{
		String result = "SARSARewards,";
		for(int i=0;i<episode;i++)
		{
			result += SARSARewards[i]+",";
		}
		result += "\nQRewards,";
		for(int i=0;i<episode;i++)
		{
			result += QRewards[i]+",";
		}
		
		result += "\nSARSALambda,";
		for(int i=0;i<episode;i++)
		{
			result += SARSALambdaRewards[i]+",";
		}
		generateFile(result, fileName);
	}
	
	/**
	 * generate reward  file to produce graph for two algorithms
	 * @param fileName
	 */
	public void generateRewardGraphFile(String fileName)
	{
		String result = "SARSAR,";
		//one bunch is 100, so there is 20 bunches
		//(it means there is 20 points for each algorithm in the graph)		
		for(int i=0;i<episode/bunchSize;i++)
		{
			double value = 0;
			for(int j=i*bunchSize;j<(i+1)*bunchSize;j++)
			{		
				double logValue = -Math.log(-SARSARewards[j]);
				value += logValue;
			}
			value /= bunchSize;
			result += value+",";
		}		
		result += "\nQ,";
		for(int i=0;i<episode/bunchSize;i++)
		{
			double value = 0;
			for(int j=i*bunchSize;j<(i+1)*bunchSize;j++)
			{
				value += -Math.log(-QRewards[j]);
			}
			value /= bunchSize;
			result += value+",";
		}		
		result += "\nSARSAR Lambda,";
		for(int i=0;i<episode/bunchSize;i++)
		{
			double value = 0;
			for(int j=i*bunchSize;j<(i+1)*bunchSize;j++)
			{
				value += -Math.log(-SARSALambdaRewards[j]);
			}
			value /= bunchSize;
			result += value+",";
		}
		generateFile(result, fileName);
	}
	
	/**
	 * clean the value of Q
	 */
	public void clean()
	{
		//clean Q value
		for(QValue qValue : Q.keySet())
		{
			Q.put(qValue, 0.0);			
		}
	}
}
